// librerias
#include <iostream>
#include <string.h>
#include <ctime>
#include <fstream>
#include <fcntl.h>
#include <pthread.h>
using namespace std;

// se establecen estructuras
struct threads_structure {
    ifstream archivo;
    int contador_lineas;
    int contador_palabras;
    int contador_caracteres;
};

// función para contar caracteres de textos
void *contarCaracteres(void *param){
    // se establecen varaibles y parametros de estructura
    threads_structure *recibir;
    recibir = (threads_structure *)param;
    int contador_caracteres = 0;
	// recorre hasta el final y cuenta caracteres
	while (recibir->archivo.good()) if (recibir->archivo.get()!=EOF) contador_caracteres++;
    // conteo de caracteres retornados
    recibir->contador_caracteres = contador_caracteres;
    cout << "Cantidad de caracteres: " << contador_caracteres << endl;
    pthread_exit(0);
}

// función cuenta palabras de textos
void *contarPalabras(void *param){
    // se establecen varaibles y parametros de estructura
    threads_structure *recibir;
    recibir = (threads_structure *)param;
    char cadena[2];
    int contador_palabras = 0;
    //mientras el archivo no termine se recorre y se suma uno al contador cuando se agregue una cadena
    while (!recibir->archivo.eof()){
        recibir->archivo >> cadena;
        contador_palabras++;
    }
    // se le decuenta uno ya que se agrega
    contador_palabras--;
    // es retornado e imprimido
    recibir->contador_palabras = contador_palabras;
    cout << "La cantidad de palabras es: " << contador_palabras << endl;
    pthread_exit(0);
}

// función contadora de lineas
void *contarLineas(void *param){
    // se establecen varaibles y parametros de estructura
    threads_structure *recibir;
    recibir = (threads_structure *)param;
    int contador_lineas = 0;
    char linea[3000];
    // mientras archivo no termine se recorre y se cuentan las lineas
    while (!recibir->archivo.eof()){
        recibir->archivo.getline(linea,3000);
        contador_lineas++;
    }
    // se retorna conteo
    recibir->contador_lineas = contador_lineas;
    cout << "La cantidad de lineas es: " << contador_lineas << endl;
    pthread_exit(0);
}

// función priincipal
int main (int argc, char* argv[]){
    //variables 
    int total_lineas = 0;
    int total_palabras = 0;
    int total_caracteres = 0;
    float t_inicial, t_final;
    // se pide ingresar valores reales
    if (argc <= 1){
        cout << "Ingrese archivos" << endl;
    }
    // se abren los hilos
    pthread_t threads[argc*3-1];
    threads_structure contenido;
    // inicia conteo de tiempo
    t_inicial = clock();
    // función que ejecuta los hilos
    for (int i=1; i<argc; i++){
        cout << "-----------------------------------------------------------------------------" << endl;
        cout << "Nombre del archivo: " << argv[i] << endl;
        
        // tres estructuras de funciones
        //cuenta lineas, abre archivo
        contenido.archivo.open(argv[i]);
        //llama a la función junto con estrucutra
        pthread_create(&threads[i-1], NULL, contarLineas, (void *)&contenido);
        pthread_join(threads[i-1], NULL);
        // suma el total
        total_lineas = total_lineas + contenido.contador_lineas;
        // cierra archivo
        contenido.archivo.close();
        
        //cuenta palabras, abre archivo
        contenido.archivo.open(argv[i]);
        //llama a la función junto con estrucutra
        pthread_create(&threads[i-1], NULL, contarPalabras, (void *)&contenido);
        pthread_join(threads[i-1], NULL);
        // suma el total
        total_palabras = total_palabras + contenido.contador_palabras;
        // cierra archivo
        contenido.archivo.close();

        //cuenta caracteres, abre archivo
        contenido.archivo.open(argv[i]);
        //llama a la función junto con estrucutra
        pthread_create(&threads[i-1], NULL, contarCaracteres, (void *)&contenido);
        pthread_join(threads[i-1], NULL);
        // suma el total
        total_caracteres = total_caracteres + contenido.contador_caracteres;
        // cierra archivo
        contenido.archivo.close();
    }
    // finaliza conteo de tiempo
    t_final = clock();
    // imprime datos obtenidos
    cout << "-----------------------------------------------------------------------------" << endl;
    cout << "Cantidad total de lineas: " << total_lineas << endl;
    cout << "Cantidad total de palabras: " << total_palabras << endl;
    cout << "Cantidad total de caracteres: " << total_caracteres << endl;
    cout << "Tiempo de ejecución: " << (t_final-t_inicial)/CLOCKS_PER_SEC << " segundos" << endl;
    return 0;
}