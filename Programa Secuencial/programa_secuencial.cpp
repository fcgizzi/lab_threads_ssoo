// librerias
#include <iostream>
#include <string.h>
#include <ctime>
#include <fstream>
#include <fcntl.h>
using namespace std;

// función que cuenta caracteres
int contarCaracteres(char* archivo){
	int contador_caracteres = 0;

	// abre archivo con nombre texto
	ifstream texto(archivo, ifstream::in);
	
	// recorre hasta el final y cuenta caracteres
	while (texto.good()) if (texto.get()!=EOF) contador_caracteres++;
	cout << "Cantidad de caracteres: " << contador_caracteres << endl; 
	// retorna cantidad de caracteres
	return contador_caracteres;
}

// función que cuenta palabras
int contarPalabras(char* archivo){
	// se establecen variables
	int contador_palabras = 0;
	char cadena[10];
	// se abre archivo
	ifstream texto(archivo);
	// recorre hasta que termine 
	while (!texto.eof()){
		texto >> cadena;
		//se cintsabiliza palabra cada vez que guarda cadena
		contador_palabras++;
	}
	// se cierra texto
	texto.close();
	// se resta uno ya que cuenta uno de más
	contador_palabras--;
	cout << "Cantidad de palabras: " << contador_palabras << endl;
	// retorna cantidad de palabras
	return contador_palabras;
}

// función que cuenta lineas
int contarLineas(ifstream &archivo){
	// se establecen variables
	int contador_lineas = 0;
	char linea[3000];
	// calcula cantidad mientras recorre archivo
	while (!archivo.eof()){
		archivo.getline(linea, 3000);
		contador_lineas++;
	}
	cout << "Cantidad de lineas: " << contador_lineas << endl;
	// se retorna cantidad de lineas
	return contador_lineas;
}
	
// función principal
int main(int argc, char* argv[]){
	// variables contadoras
	int total_lineas = 0;
	int total_palabras = 0;
	int total_caracteres = 0;
	// variable de tiempo
	float t_inicial, t_final;
	// se deben ingresar valores reales
	if (argc <= 1){
		cout << "Ingrese nombre de los archivos de datos" << endl;
	}
	ifstream archivo; // almacenar archivos de texto
	// inicia el tiempo
	t_inicial = clock();
	for (int i=1; i<argc; i++){
		cout << "-----------------------------------------------------------------------------" << endl;
		cout << "Nombre de archivo: " << argv[i] << endl;
		// se abre archivo
		archivo.open(argv[i]);
		// contar cantidad de lineas 
		total_lineas = total_lineas + contarLineas(archivo);
		archivo.close();
		// contar cantidad de palabras
		total_palabras = total_palabras + contarPalabras(argv[i]);
		total_caracteres = total_caracteres + contarCaracteres(argv[i]);
		//termina el ciclo de tiempo
	}
	// finaliza el conteo de tiempo
	t_final = clock();
	// resultados entregados
	cout << "-----------------------------------------------------------------------------" << endl;
	cout << "Cantidad total de lineas: " << total_lineas << endl;
	cout << "Cantidad total de palabras: " << total_palabras << endl;	
	cout << "Cantidad total de caracteres: " << total_caracteres << endl;
	cout << "Tiempo de ejecución: " << (t_final-t_inicial)/CLOCKS_PER_SEC << " segundos" << endl;
	return 0;	
}
