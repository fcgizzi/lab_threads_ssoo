# Laboratorio 3 Unidad II SSOO y redes

## Contador de palabras, lineas y caracteres de textos

Se pide un programa en lenguaje C++ que reciba a través de argumentos de una lista de archivo y cuente las líneas, palabras y caracteres qye hay en cada uno de los archivos de exto, además del total de cada uno de ellos. También se pide medir el tiempo que emplea el programa en realizar el proceso.

## Requisitos

Para el funcionamiento del programa se solicita:

- Un computador
- Sistema operativo de preferencia con alguna distribución de linux
- Tener instalado Make
    - Si no cuenta con make, se instala con el siguiente comando: 
        
        `sudo apt install make`


## Compilación y ejecución

Para compilar se debe utilizar la terminal de linux, ingresando `g++ programa_secuencial.cpp -o programa programa_secuencial`, o también, teniendo make instalado previamente, utilizar el comando `make`.

Para ejecutar se accede al programa desde la terminal: `./programa_secuencial {archivo 1} {archivo 2} ... {archivo n}`.

(Existen dos archivos en el directorio para realizar pruebas)


## Programa construido con

- Distribución de Linux, Ubuntu
- Geany, programa para editar y programar
- Lenguaje de progrmación C++
- Librerias iostream, string.h, ctime, fstream y fcntl.h


## Autor

Franco Cifuentes Gizzi - fcifuentes19@alumnos.utalca.cl
